#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
import sys, getopt

def main(argv):
   git_project_name=''
   git_url = ''
   git_branch = ''
   git_compile_mode=''
   git_build_number=0
   git_commit_uuid=''
   try:
      opts, args = getopt.getopt(argv,"hu:b:m:n:c:p:",["url=","branch=","mode=","number=","commit=","project_name="])
   except getopt.GetoptError:
      print 'build_cpp.py -u <git url> -b <git branch> -m <mode debug or release> -n <build number> -c <commit uuid> -p <project name>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'build_cpp.py -u <git url> -b <git branch> -m <mode debug or release> -n <build number> -c <commit uuid>  -p <project name>'
         sys.exit()
      elif opt in ("-u", "--url"):
         git_url = arg
      elif opt in ("-b", "--branch"):
         git_branch = arg
      elif opt in ("-m", "--mode"):
         git_compile_mode = arg
      elif opt in ("-n", "--number"):
         git_build_number = arg
      elif opt in ("-c", "--commit"):
         git_commit_uuid = arg
      elif opt in ("-p", "--project_name"):
         git_project_name = arg
   print('git url:',git_url ,' branch:',git_branch,' compile mode:',git_compile_mode , "build number:",git_build_number , "commit uuid" , git_commit_uuid,'project name:',git_project_name)
   
 
   dirname=git_project_name+'-'+git_branch+'-'+git_compile_mode+'-'+git_build_number
   workdir = os.path.join(os.getcwd(), dirname)
   print(workdir)
   try:
      os.mkdir(workdir)
   except WindowsError:
      pass
   os.chdir(workdir)
   print('pwd',os.getcwd())
   print('workdir=',workdir)
   
   git_url=git_url.replace('http://','http://d3:7dcypf-3EyYy3YGNyByz@')
   print('git url:',git_url)
   os.system('git clone '+git_url)
   print("git_project_name"+git_project_name)
   os.chdir(git_project_name)
   os.system('git checkout '+git_branch)
   os.system('git push')  
   os.mkdir(git_compile_mode)
   os.chdir(git_compile_mode)
   
   filename = 'build.bat'
   with open(filename, 'w') as f:
      f.write('call "C:/Program Files (x86)/Microsoft Visual Studio 12.0/VC/vcvarsall.bat" amd64\r\n')
      f.write('cmake .. -G "Visual Studio 12 2013" -A x64 -DCMAKE_INSTALL_PREFIX=./install -DFREECAD_LIBPACK_DIR="D:\\repos\\FreeCADLibs_11.11_x64_VC12"  -DBUILD_SPREADSHEET:BOOL="1" -DBUILD_COMPLETE:BOOL="0" -DBUILD_ROBOT:BOOL="0" -DBUILD_SKETCHER:BOOL="1" -DBUILD_PLOT:BOOL="0" -DBUILD_SHIP:BOOL="0" -DBUILD_SHOW:BOOL="0" -DBUILD_DRAWING:BOOL="1" -DFREECAD_USE_QT_FILEDIALOG:BOOL="1" -DBUILD_FEM:BOOL="0" -DBUILD_TEST:BOOL="0" -DBUILD_MESH:BOOL="0" -DBUILD_POINTS:BOOL="0" -DBUILD_INSPECTION:BOOL="0" -DBUILD_QT5_WEBKIT:BOOL="0" -DBUILD_IDF:BOOL="0" -DBUILD_FEM_NETGEN:BOOL="0" -DBUILD_ARCH:BOOL="0" -DBUILD_TUX:BOOL="0" -DBUILD_DRAFT:BOOL="1" -DOCC_INCLUDE_DIR:PATH="D:/repos/FreeCADLibs_11.11_x64_VC12/include/opencascade" -DBUILD_ADDONMGR:BOOL="0" -DBUILD_RAYTRACING:BOOL="0" -DBUILD_OPENSCAD:BOOL="0" -DOCE_DIR:PATH="OCE_DIR-NOTFOUND" -DBUILD_IMAGE:BOOL="0" -DBUILD_PATH:BOOL="0" -DBUILD_MATERIAL:BOOL="0" -DOCC_LIBRARY:FILEPATH="D:/repos/FreeCADLibs_11.11_x64_VC12/lib/TKernel.lib" -DBUILD_MESH_PART:BOOL="0" -DBUILD_REVERSEENGINEERING:BOOL="0"\r\n')
      f.write('msbuild ALL_BUILD.vcxproj /p:Configuration='+git_compile_mode+'\r\n')
      f.write('msbuild INSTALL.vcxproj\r\n')
   
   os.system("build.bat")
   
   #os.system('rmdir /S /Q D:\\repos\install_maker/FreeCAD/installer/FreeCAD')
   os.system('xcopy /C /Y /S /I D:\\repos\\install_maker\\FreeCAD\\installer\\libpacks\\bin D:\\repos\\install_maker\\FreeCAD\\installer\\FreeCAD\\'+dirname)
   os.system('xcopy /C /Y /S /I install D:\\repos\\install_maker\\FreeCAD\\installer\\FreeCAD\\'+dirname)
   str_setup_nsh='''/*
Settings for FreeCAD installer

These typically need to be modified for each FreeCAD release

*/

# Make the installer as small as possible
# comment this for testing builds since it will reduce the time to create an installer
# a lot - for the cost of a much greater file size.
# So assure it is active for release builds!
SetCompressor /SOLID lzma

#--------------------------------
# Version number

!define APP_VERSION_MAJOR @APP_VERSION_MAJOR@
!define APP_VERSION_MINOR @APP_VERSION_MINOR@
!define APP_VERSION_REVISION @APP_VERSION_REVISION@
!define APP_VERSION_EMERGENCY "@APP_VERSION_EMERGENCY@" # use "1" for an emergency release of FreeCAD otherwise ""
	# alternatively you can use APP_VERSION_EMERGENCY for a custom suffix of the version number
!define APP_EMERGENCY_DOT "" # use "." for an emergency release of FreeCAD otherwise ""
!define APP_VERSION_BUILD "@APP_VERSION_BUILD@" # Start with 1 for the installer releases of each version

!define APP_VERSION "${APP_VERSION_MAJOR}.${APP_VERSION_MINOR}.${APP_VERSION_REVISION}${APP_EMERGENCY_DOT}${APP_VERSION_EMERGENCY}" # Version to display

!define COPYRIGHT_YEAR 2019

#--------------------------------
# Installer file name
# Typical names for the release are "FreeCAD-018-Installer-1.exe" etc.

!define ExeFile "${APP_NAME}-${APP_VERSION_MAJOR}${APP_VERSION_MINOR}${APP_VERSION_REVISION}${APP_VERSION_EMERGENCY}-Installer-${APP_VERSION_BUILD}.exe"

#--------------------------------
# installer bit type - for a 32bit or 64bit FreeCAD

# just comment this line for a 32bit installer:
!define MULTIUSER_USE_PROGRAMFILES64

#--------------------------------
# File locations
# !!! you need to adjust them to the folders in your Windows system !!!

!define FILES_FREECAD "@FILES_FREECAD@"
!define FILES_DEPS "@FILE_DIR@\\FreeCAD\\Installer\\MSVCRedist"
!define FILES_THUMBS "@FILE_DIR@\\FreeCAD\\installer\\thumbnail"
'''

   APP_VERSION_MAJOR='1'
   APP_VERSION_MINOR='0'
   APP_VERSION_REVISION=str(git_build_number)
   APP_VERSION_EMERGENCY=str(git_branch)+'-'+str(git_compile_mode)
   APP_VERSION_BUILD=str(git_build_number)
   exe_file_name='FreeCAD-'+APP_VERSION_MAJOR+APP_VERSION_MINOR+APP_VERSION_REVISION+APP_VERSION_EMERGENCY+'-Installer-'+APP_VERSION_BUILD
   
   str_setup_nsh=str_setup_nsh.replace('@APP_VERSION_MAJOR@', APP_VERSION_MAJOR)
   str_setup_nsh=str_setup_nsh.replace('@APP_VERSION_MINOR@', APP_VERSION_MINOR)
   str_setup_nsh=str_setup_nsh.replace('@APP_VERSION_REVISION@', APP_VERSION_REVISION)
   str_setup_nsh=str_setup_nsh.replace('@APP_VERSION_EMERGENCY@', APP_VERSION_EMERGENCY)
   str_setup_nsh=str_setup_nsh.replace('@APP_VERSION_BUILD@', APP_VERSION_BUILD)
   str_setup_nsh=str_setup_nsh.replace('@FILE_DIR@', 'D:\\repos\\install_maker')
   str_setup_nsh=str_setup_nsh.replace('@FILES_FREECAD@', 'D:\\repos\\install_maker\\FreeCAD\\Installer\\FreeCAD\\'+dirname+'\\')
   

   filename = 'D:\\repos\\install_maker\\FreeCADInstProj\\Settings.nsh'
   with open(filename, 'w') as f:
      f.write(str_setup_nsh)
      f.write('\r\n')

   os.system('"C:\\Program Files (x86)\\NSIS\\makensis.exe"   D:\\repos\\install_maker\\FreeCADInstProj\\FreeCAD-installer.nsi')
   
   #os.system('xcopy \\C \\Q \\Y \\S D:\\repos\\install_maker\\FreeCADInstProj\\'+exe_file_name+'.exe  D:\\repos\\install_maker\\packages\\')   
   
   str_ftp_cmd='''
   open 192.168.1.3
e
123
cd /freecad_installer_pacakage
prompt off
mput D:\\repos\\install_maker\\FreeCADInstProj\\@exe_file_name@.exe
bye
quit
exit
   '''
   str_ftp_cmd=str_ftp_cmd.replace('@exe_file_name@',exe_file_name)
   ftp_cmd = 'ftp_cmd.txt'
   with open(ftp_cmd, 'w') as f:
      f.write(str_ftp_cmd)
      f.write('\r\n')
      
   os.system('ftp -s:'+ftp_cmd)
   
if __name__ == "__main__":
   main(sys.argv[1:])
